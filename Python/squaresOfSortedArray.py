def sorted_squares(A):
    new_list = []
    for x in A:
        new_list.append(x**2)

    new_list.sort()
    return new_list


print(sorted_squares([-4, -1, 0, 3, 10]))